let accounts_menu = $("#batch_connect_session_context_slurm_account");
let partitions_menu = $("#batch_connect_session_context_slurm_partition");
let gpus_menu = $("#batch_connect_session_context_gres_gpu");

function update_partitions() {
    let account = accounts_menu.val();
    let partition = partitions_menu.val();

    $("#batch_connect_session_context_slurm_partition > option").each(function() {
        if($(this).data('accounts').includes(account)) {
            $(this).removeAttr('disabled');
        }
        else
        {
            $(this).attr('disabled','disabled');
            if($(this).val() == partition) {
                $(this).prop('selected', false);
            }
        }
    });

    update_gpus();
}

function update_gpus() {
    let partition = partitions_menu.val();
    let gpu = gpus_menu.val();

    let any_available = false;

    $("#batch_connect_session_context_gres_gpu > option").each(function() {
        if($(this).val() === '' || $(this).data('partitions').includes(partition)) {
            $(this).removeAttr('disabled');
	    if($(this).val() != '') {
	        any_available = true;
	    }
        } else {
            $(this).attr('disabled','disabled');
            if($(this).val() == gpu) {
                $(this).prop('selected', false);
            }
        }
    });
    
    let first_option = $("#batch_connect_session_context_gres_gpu > option:first-child");
    if(any_available) {
        first_option.text("Use any GPU available");
	first_option.css("color", "");
    }
    else {
        first_option.text("No GPU available");
	first_option.css("color", "#FF0000");
    }
}

accounts_menu.change(update_partitions);
partitions_menu.change(update_gpus);
update_partitions();
